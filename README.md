## Introduction

The Observer design pattern is a behavioral design pattern that defines a one-to-many relationship between objects, so that when one object (the subject or publisher) changes its state, all its dependents (observers or subscribers) are notified and updated automatically. In .NET Core 7, you can implement the Observer pattern to create a system where objects can observe and react to changes in another object. Here's how you can implement it in .NET Core 7:

First, let's define the components of the Observer pattern:

Subject (Publisher): The object that is being observed. It maintains a list of observers and notifies them of state changes.

Observer (Subscriber): The objects that are interested in the state changes of the subject. They register with the subject and receive updates when the subject's state changes.

The deme is how to implement the Observer pattern in .NET Core 7 using C#:

In this example, we've defined the `Subject` class, two concrete `Observer` classes (`ConcreteObserverA` and `ConcreteObserverB`), and an `IObserver` interface. The `Subject` maintains a list of observers and notifies them when it undergoes a state change. The observers implement the `Update` method to react to these state changes.

When you run the program, you'll see that both observer objects receive notifications when the `DoSomething` method of the `Subject` is called, demonstrating the Observer pattern in action.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [.NET Hello World tutorial](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/)

If you're new to .NET you'll want to check out the tutorial, but if you're
already a seasoned developer considering building your own .NET app with GitLab,
this should all look very familiar.

## What's contained in this project

The root of the repository contains the out of the `dotnet new console` command,
which generates a new console application that just prints out "Hello, World."
It's a simple example, but great for demonstrating how easy GitLab CI is to
use with .NET. Check out the `Program.cs` and `dotnetcore.csproj` files to
see how these work.

In addition to the .NET Core content, there is a ready-to-go `.gitignore` file
sourced from the the .NET Core [.gitignore](https://github.com/dotnet/core/blob/master/.gitignore). This
will help keep your repository clean of build files and other configuration.

Finally, the `.gitlab-ci.yml` contains the configuration needed for GitLab
to build your code. Let's take a look, section by section.

First, we note that we want to use the official Microsoft .NET SDK image
to build our project.

```
image: microsoft/dotnet:latest
```

We're defining two stages here: `build`, and `test`. As your project grows
in complexity you can add more of these.

```
stages:
    - build
    - test
```

Next, we define our build job which simply runs the `dotnet build` command and
identifies the `bin` folder as the output directory. Anything in the `bin` folder
will be automatically handed off to future stages, and is also downloadable through
the web UI.

```
build:
    stage: build
    script:
        - "dotnet build"
    artifacts:
      paths:
        - bin/
```

Similar to the build step, we get our test output simply by running `dotnet test`.

```
test:
    stage: test
    script: 
        - "dotnet test"
```

This should be enough to get you started. There are many, many powerful options 
for your `.gitlab-ci.yml`. You can read about them in our documentation 
[here](https://docs.gitlab.com/ee/ci/yaml/).

## Developing with Gitpod

This template repository also has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

The `.gitpod.yml` ensures that, when you open this repository in Gitpod, you'll get a cloud workspace with .NET Core pre-installed, and your project will automatically be built and start running.