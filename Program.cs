﻿using System;
using System.Collections.Generic;

// Define the Observer (Subscriber) interface
public interface IObserver
{
    void Update(string message);
}

// Concrete Observer implementations
public class ConcreteObserverA : IObserver
{
    public void Update(string message)
    {
        Console.WriteLine($"Observer A received message: {message}");
    }
}

public class ConcreteObserverB : IObserver
{
    public void Update(string message)
    {
        Console.WriteLine($"Observer B received message: {message}");
    }
}

// Define the Subject (Publisher)
public class Subject
{
    private List<IObserver> observers = new List<IObserver>();

    public void RegisterObserver(IObserver observer)
    {
        observers.Add(observer);
    }

    public void RemoveObserver(IObserver observer)
    {
        observers.Remove(observer);
    }

    public void NotifyObservers(string message)
    {
        foreach (var observer in observers)
        {
            observer.Update(message);
        }
    }

    public void DoSomething()
    {
        // ... Some action that changes the subject's state

        // Notify observers
        NotifyObservers("State has changed.");
    }
}

public class Program
{
    public static void Main()
    {
        // Create subject and observers
        var subject = new Subject();
        var observerA = new ConcreteObserverA();
        var observerB = new ConcreteObserverB();

        // Register observers with the subject
        subject.RegisterObserver(observerA);
        subject.RegisterObserver(observerB);

        // Trigger a state change in the subject
        subject.DoSomething();
    }
}
